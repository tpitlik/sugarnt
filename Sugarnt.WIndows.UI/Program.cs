﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sugarnt.WIndows.UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            SaveSettingsFirstTime();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        static void SaveSettingsFirstTime()
        {
            var settings = new Settings
            {
                MinCalories = 0,
                MaxCalories = 250,
                MinFats = 0,
                MaxFats = 9.5,
                MinCarbohydrates = 0,
                MaxCarbohydrates = 70,
                Banned = new List<string>
                {
                    "Сахар",
                    "Сахар1",
                    "Сахар2",
                    "Сахар3",
                    "Сахар4",
                    "Сахар5",
                    "Сахар6",
                    "Сахар7",
                    "Сахар8",
                    "Сахар9",
                    "Сахар10"
                }
            };

            settings.Save();
        }
    }
}
