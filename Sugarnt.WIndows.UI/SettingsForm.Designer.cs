﻿namespace Sugarnt.WIndows.UI
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Button DeleteProduct;
            System.Windows.Forms.Button AddProduct;
            this.MaxCaloriesTB = new System.Windows.Forms.TextBox();
            this.MinCaloriesTB = new System.Windows.Forms.TextBox();
            this.MaxProteinesTB = new System.Windows.Forms.TextBox();
            this.MaxProteines = new System.Windows.Forms.Label();
            this.MinProteinesTB = new System.Windows.Forms.TextBox();
            this.MinProteines = new System.Windows.Forms.Label();
            this.MaxFatsTB = new System.Windows.Forms.TextBox();
            this.MaxFats = new System.Windows.Forms.Label();
            this.MinFatsTB = new System.Windows.Forms.TextBox();
            this.MaxCarbohydratesTB = new System.Windows.Forms.TextBox();
            this.MaxCarbohydrates = new System.Windows.Forms.Label();
            this.MinCarbohydratesTB = new System.Windows.Forms.TextBox();
            this.MinCarbohydrates = new System.Windows.Forms.Label();
            this.MinFats = new System.Windows.Forms.Label();
            this.MaxCalories = new System.Windows.Forms.Label();
            this.MinCalories = new System.Windows.Forms.Label();
            this.Products = new System.Windows.Forms.ListBox();
            this.AddProductTB = new System.Windows.Forms.TextBox();
            this.SaveSettings = new System.Windows.Forms.Button();
            DeleteProduct = new System.Windows.Forms.Button();
            AddProduct = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DeleteProduct
            // 
            DeleteProduct.Location = new System.Drawing.Point(234, 307);
            DeleteProduct.Name = "DeleteProduct";
            DeleteProduct.Size = new System.Drawing.Size(165, 43);
            DeleteProduct.TabIndex = 3;
            DeleteProduct.Text = "Delete Product";
            DeleteProduct.UseVisualStyleBackColor = true;
            DeleteProduct.Click += new System.EventHandler(this.DeleteProduct_Click);
            // 
            // AddProduct
            // 
            AddProduct.Location = new System.Drawing.Point(234, 203);
            AddProduct.Name = "AddProduct";
            AddProduct.Size = new System.Drawing.Size(165, 37);
            AddProduct.TabIndex = 3;
            AddProduct.Text = "Add Product";
            AddProduct.UseVisualStyleBackColor = true;
            AddProduct.Click += new System.EventHandler(this.AddProduct_Click);
            // 
            // MaxCaloriesTB
            // 
            this.MaxCaloriesTB.Location = new System.Drawing.Point(12, 57);
            this.MaxCaloriesTB.Name = "MaxCaloriesTB";
            this.MaxCaloriesTB.Size = new System.Drawing.Size(144, 20);
            this.MaxCaloriesTB.TabIndex = 0;
            // 
            // MinCaloriesTB
            // 
            this.MinCaloriesTB.Location = new System.Drawing.Point(12, 139);
            this.MinCaloriesTB.Name = "MinCaloriesTB";
            this.MinCaloriesTB.Size = new System.Drawing.Size(144, 20);
            this.MinCaloriesTB.TabIndex = 0;
            // 
            // MaxProteinesTB
            // 
            this.MaxProteinesTB.Location = new System.Drawing.Point(184, 57);
            this.MaxProteinesTB.Name = "MaxProteinesTB";
            this.MaxProteinesTB.Size = new System.Drawing.Size(144, 20);
            this.MaxProteinesTB.TabIndex = 0;
            // 
            // MaxProteines
            // 
            this.MaxProteines.AutoSize = true;
            this.MaxProteines.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaxProteines.Location = new System.Drawing.Point(204, 19);
            this.MaxProteines.Name = "MaxProteines";
            this.MaxProteines.Size = new System.Drawing.Size(109, 20);
            this.MaxProteines.TabIndex = 1;
            this.MaxProteines.Text = "Max Proteines";
            // 
            // MinProteinesTB
            // 
            this.MinProteinesTB.Location = new System.Drawing.Point(184, 139);
            this.MinProteinesTB.Name = "MinProteinesTB";
            this.MinProteinesTB.Size = new System.Drawing.Size(144, 20);
            this.MinProteinesTB.TabIndex = 0;
            // 
            // MinProteines
            // 
            this.MinProteines.AutoSize = true;
            this.MinProteines.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinProteines.Location = new System.Drawing.Point(204, 101);
            this.MinProteines.Name = "MinProteines";
            this.MinProteines.Size = new System.Drawing.Size(105, 20);
            this.MinProteines.TabIndex = 1;
            this.MinProteines.Text = "Min Proteines";
            // 
            // MaxFatsTB
            // 
            this.MaxFatsTB.Location = new System.Drawing.Point(357, 57);
            this.MaxFatsTB.Name = "MaxFatsTB";
            this.MaxFatsTB.Size = new System.Drawing.Size(144, 20);
            this.MaxFatsTB.TabIndex = 0;
            // 
            // MaxFats
            // 
            this.MaxFats.AutoSize = true;
            this.MaxFats.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaxFats.Location = new System.Drawing.Point(390, 19);
            this.MaxFats.Name = "MaxFats";
            this.MaxFats.Size = new System.Drawing.Size(74, 20);
            this.MaxFats.TabIndex = 1;
            this.MaxFats.Text = "Max Fats";
            // 
            // MinFatsTB
            // 
            this.MinFatsTB.Location = new System.Drawing.Point(357, 139);
            this.MinFatsTB.Name = "MinFatsTB";
            this.MinFatsTB.Size = new System.Drawing.Size(144, 20);
            this.MinFatsTB.TabIndex = 0;
            // 
            // MaxCarbohydratesTB
            // 
            this.MaxCarbohydratesTB.Location = new System.Drawing.Point(543, 57);
            this.MaxCarbohydratesTB.Name = "MaxCarbohydratesTB";
            this.MaxCarbohydratesTB.Size = new System.Drawing.Size(144, 20);
            this.MaxCarbohydratesTB.TabIndex = 0;
            // 
            // MaxCarbohydrates
            // 
            this.MaxCarbohydrates.AutoSize = true;
            this.MaxCarbohydrates.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaxCarbohydrates.Location = new System.Drawing.Point(539, 19);
            this.MaxCarbohydrates.Name = "MaxCarbohydrates";
            this.MaxCarbohydrates.Size = new System.Drawing.Size(149, 20);
            this.MaxCarbohydrates.TabIndex = 1;
            this.MaxCarbohydrates.Text = "Max CarboHydrates";
            // 
            // MinCarbohydratesTB
            // 
            this.MinCarbohydratesTB.Location = new System.Drawing.Point(543, 139);
            this.MinCarbohydratesTB.Name = "MinCarbohydratesTB";
            this.MinCarbohydratesTB.Size = new System.Drawing.Size(144, 20);
            this.MinCarbohydratesTB.TabIndex = 0;
            // 
            // MinCarbohydrates
            // 
            this.MinCarbohydrates.AutoSize = true;
            this.MinCarbohydrates.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinCarbohydrates.Location = new System.Drawing.Point(542, 101);
            this.MinCarbohydrates.Name = "MinCarbohydrates";
            this.MinCarbohydrates.Size = new System.Drawing.Size(142, 20);
            this.MinCarbohydrates.TabIndex = 1;
            this.MinCarbohydrates.Text = "Min Carbohydrates";
            // 
            // MinFats
            // 
            this.MinFats.AutoSize = true;
            this.MinFats.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinFats.Location = new System.Drawing.Point(394, 101);
            this.MinFats.Name = "MinFats";
            this.MinFats.Size = new System.Drawing.Size(70, 20);
            this.MinFats.TabIndex = 1;
            this.MinFats.Text = "Min Fats";
            // 
            // MaxCalories
            // 
            this.MaxCalories.AutoSize = true;
            this.MaxCalories.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaxCalories.Location = new System.Drawing.Point(37, 19);
            this.MaxCalories.Name = "MaxCalories";
            this.MaxCalories.Size = new System.Drawing.Size(99, 20);
            this.MaxCalories.TabIndex = 1;
            this.MaxCalories.Text = "Max Calories";
            // 
            // MinCalories
            // 
            this.MinCalories.AutoSize = true;
            this.MinCalories.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinCalories.Location = new System.Drawing.Point(37, 101);
            this.MinCalories.Name = "MinCalories";
            this.MinCalories.Size = new System.Drawing.Size(95, 20);
            this.MinCalories.TabIndex = 1;
            this.MinCalories.Text = "Min Calories";
            // 
            // Products
            // 
            this.Products.FormattingEnabled = true;
            this.Products.Location = new System.Drawing.Point(24, 203);
            this.Products.Name = "Products";
            this.Products.Size = new System.Drawing.Size(173, 147);
            this.Products.TabIndex = 2;
            // 
            // AddProductTB
            // 
            this.AddProductTB.Location = new System.Drawing.Point(234, 264);
            this.AddProductTB.Name = "AddProductTB";
            this.AddProductTB.Size = new System.Drawing.Size(165, 20);
            this.AddProductTB.TabIndex = 0;
            // 
            // SaveSettings
            // 
            this.SaveSettings.Location = new System.Drawing.Point(439, 203);
            this.SaveSettings.Name = "SaveSettings";
            this.SaveSettings.Size = new System.Drawing.Size(245, 147);
            this.SaveSettings.TabIndex = 4;
            this.SaveSettings.Text = "Save Settings";
            this.SaveSettings.UseVisualStyleBackColor = true;
            this.SaveSettings.Click += new System.EventHandler(this.SaveSettings_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 378);
            this.Controls.Add(this.SaveSettings);
            this.Controls.Add(AddProduct);
            this.Controls.Add(DeleteProduct);
            this.Controls.Add(this.Products);
            this.Controls.Add(this.MinCarbohydrates);
            this.Controls.Add(this.MinCalories);
            this.Controls.Add(this.MinProteines);
            this.Controls.Add(this.MinCarbohydratesTB);
            this.Controls.Add(this.MinFatsTB);
            this.Controls.Add(this.AddProductTB);
            this.Controls.Add(this.MinProteinesTB);
            this.Controls.Add(this.MinCaloriesTB);
            this.Controls.Add(this.MaxCarbohydrates);
            this.Controls.Add(this.MinFats);
            this.Controls.Add(this.MaxFats);
            this.Controls.Add(this.MaxCalories);
            this.Controls.Add(this.MaxProteines);
            this.Controls.Add(this.MaxCarbohydratesTB);
            this.Controls.Add(this.MaxFatsTB);
            this.Controls.Add(this.MaxProteinesTB);
            this.Controls.Add(this.MaxCaloriesTB);
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox MaxCaloriesTB;
        private System.Windows.Forms.TextBox MinCaloriesTB;
        private System.Windows.Forms.TextBox MaxProteinesTB;
        private System.Windows.Forms.Label MaxProteines;
        private System.Windows.Forms.TextBox MinProteinesTB;
        private System.Windows.Forms.Label MinProteines;
        private System.Windows.Forms.TextBox MaxFatsTB;
        private System.Windows.Forms.Label MaxFats;
        private System.Windows.Forms.TextBox MinFatsTB;
        private System.Windows.Forms.TextBox MaxCarbohydratesTB;
        private System.Windows.Forms.Label MaxCarbohydrates;
        private System.Windows.Forms.TextBox MinCarbohydratesTB;
        private System.Windows.Forms.Label MinCarbohydrates;
        private System.Windows.Forms.Label MinFats;
        private System.Windows.Forms.Label MaxCalories;
        private System.Windows.Forms.Label MinCalories;
        private System.Windows.Forms.ListBox Products;
        private System.Windows.Forms.TextBox AddProductTB;
        private System.Windows.Forms.Button SaveSettings;
    }
}