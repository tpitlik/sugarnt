﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using Sugarnt.Logic;

namespace Sugarnt.WIndows.UI
{
    public partial class MainForm : Form
    {
        private Settings _settings;
        private ProductAnalyzer _productAnalyzer;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            _settings = Settings.Load();
            if (_settings is null)
            {
                throw new ArgumentNullException("Cannot load settings");
            }

            _productAnalyzer = new ProductAnalyzer(_settings);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _settings.Save();
        }

        private async void BarcodeText_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
            {
                //MessageBox.Show($"Barcode {barcodeText.Text} scanned");
                //processBarcode(barcodeText.Text);

                var task = ProcessProductByBarcode(barcodeText.Text);

                barcodeText.Text = "";

                var errors = await task;
            }
        }

        private async Task<IEnumerable<AnalyzeError>> ProcessProductByBarcode(string barcode)
        {
            var provider = new EdostavkaProductProvider();
            var product = await provider.GetProductByBarcode(barcode);
            return _productAnalyzer.Analyze(product);
        }

        private void SettingsButton_Click(object sender, EventArgs e)
        {
            var settingsForm = new SettingsForm(_settings);
            settingsForm.Show();
        }


    }
}
