﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using System.Configuration;

namespace Sugarnt.WIndows.UI
{
    [Serializable]
    public class Settings
    {
        public List<string> Banned { get; set; }

        public double MinCalories { get; set; }
        public double MaxCalories { get; set; }

        public double MinProteines { get; set; }
        public double MaxProteines { get; set; }

        public double MinFats { get; set; }
        public double MaxFats { get; set; }

        public double MinCarbohydrates { get; set; }
        public double MaxCarbohydrates { get; set; }

        public Settings()
        {
        }

        public static Settings Load()
        {
            AppSettingsReader apr = new AppSettingsReader();
            var settingsPath = (string)apr.GetValue("SettingsPath", typeof(string));

            XmlSerializer serializer = new XmlSerializer(typeof(Settings));

            Settings settings = null;
            using (FileStream stream = File.Open(settingsPath, FileMode.Open, FileAccess.Read))
            {
                settings = serializer.Deserialize(stream) as Settings;
            }
            return settings;
        }

        public void Save()
        {
            AppSettingsReader apr = new AppSettingsReader();
            var settingsPath = (string)apr.GetValue("SettingsPath", typeof(string));

            XmlSerializer serializer = new XmlSerializer(this.GetType());
            File.Delete(settingsPath);

            using (FileStream stream = File.Open(settingsPath, FileMode.OpenOrCreate, FileAccess.Write))
            {
                serializer.Serialize(stream, this);
            }
        }
    }
}
