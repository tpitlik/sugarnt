﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom.Html;

namespace Sugarnt.Logic
{
    public class EdostavkaProductProvider:IPrductProvider
    {
        public Product GetProductByName(string productName)
        {
            throw new NotImplementedException();
        }

        public async Task<Product> GetProductByBarcode(string barcode)
        {
            var config = Configuration.Default.WithDefaultLoader();

            var address = $"https://e-dostavka.by/search/?searchtext={barcode}#nop";
            var document = await BrowsingContext.New(config).OpenAsync(address);
            var imagesSelectors = document.QuerySelectorAll("img");

            var imgLink = "";
            foreach (IHtmlImageElement img in imagesSelectors)
            {
                if (img.ClassName == "retina_redy")
                {
                    imgLink = img.Source;
                    break;;
                }
            }


            var hiddenItems = document.QuerySelectorAll("input");

            string product_id = "";
            foreach (IHtmlInputElement item in (hiddenItems))
            {
                string name = item.Name;
                if (item.Name == "product_id")
                {
                    product_id = item.Value;
                    break;
                }
            }

            address = $"https://e-dostavka.by/catalog/item_{product_id}.html";
            document = await BrowsingContext.New(config).OpenAsync(address);
            var trItems = document.QuerySelectorAll("tr");

            string content = "";
            foreach (IHtmlTableRowElement row in trItems)
            {
                if (row.ClassName == "property_3220")
                {
                    var cells = row.Cells;
                    content = cells[1].TextContent;
                    break;
                }
            }

            var h1Item = document.QuerySelectorAll("h1").FirstOrDefault();
            string productName = h1Item.TextContent;

            string proteinContent = "";
            string fatContent = "";
            string sugarsContent = "";
            string calories = "";

            foreach (IHtmlTableRowElement row in trItems)
            {
                if (row.ClassName == "property_307")
                {
                    var cells = row.Cells;
                    proteinContent = cells[1].TextContent;
                }
                else if (row.ClassName == "property_308")
                {
                    var cells = row.Cells;
                    fatContent = cells[1].TextContent;
                }
                else if (row.ClassName == "property_317")
                {
                    var cells = row.Cells;
                    sugarsContent = cells[1].TextContent;
                }else if (row.ClassName == "property_313")
                {
                    var cells = row.Cells;
                    calories = cells[1].TextContent;
                    calories = calories.Split('/')[0];
                    calories = calories.Split(' ')[0];
                }
            }

            Product product = new Product();

            product.Name = productName;
            product.Content = content;
            product.ImageUrl = imgLink;

            //var t = proteinContent.Split(' ')[0];
            //double.Parse(t, CultureInfo.InvariantCulture);

            try
            {
                product.Values.Proteines = double.Parse(proteinContent.Split(' ')[0], CultureInfo.InvariantCulture);
                product.Values.Fats = double.Parse(fatContent.Split(' ')[0], CultureInfo.InvariantCulture);
                product.Values.Carbohydrates = double.Parse(sugarsContent.Split(' ')[0], CultureInfo.InvariantCulture);
                product.Values.Calories = double.Parse(calories, CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {

            }

            return product;
        }
    }
}
