﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sugarnt.WIndows.UI;

namespace Sugarnt.Logic
{
    public class ProductAnalyzer
    {
        private Settings _settings;

        public ProductAnalyzer(Settings setting)
        {
            _settings = setting;
        }

        public IEnumerable<AnalyzeError> Analyze(Product product)
        {
            var result = new List<AnalyzeError>();


            var productContent = product.Content.Split(',').Select(s=> s.Trim());

            foreach (string contentItem in productContent.Intersect(_settings.Banned.Select(s => s.Trim()), StringComparer.OrdinalIgnoreCase))
            {
                result.Add(AnalyzeError.BanedError($"Product contains \"Banned content\" - {contentItem}"));
            }

            CheckRangeValueError(result, product.Values.Proteines, "Proteines", _settings.MinProteines, _settings.MaxProteines);
            CheckRangeValueError(result, product.Values.Carbohydrates, "Carbohydrates", _settings.MinCarbohydrates, _settings.MaxCarbohydrates);
            CheckRangeValueError(result, product.Values.Fats, "Fats", _settings.MinFats, _settings.MaxFats);
            CheckRangeValueError(result, product.Values.Calories, "Calories", _settings.MinCalories, _settings.MaxCalories);

            return result;
        }

        private void CheckRangeValueError(IList<AnalyzeError> errorList, double value, string valueName, double min, double max)
        {
            if (value >= max || value <= min)
            {
                errorList.Add(AnalyzeError.ValueError($"{valueName} - {value}, outside the allowable range [{min}:{max}]"));
            }
        }
    }
}
