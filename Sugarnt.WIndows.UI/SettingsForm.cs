﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Sugarnt.WIndows.UI
{
    public partial class SettingsForm : Form
    {
        private Settings _settings;

        public SettingsForm(Settings settings)
        {
            InitializeComponent();

            _settings = settings;

            MaxCaloriesTB.Text = _settings.MaxCalories.ToString();
            MinCaloriesTB.Text = _settings.MinCalories.ToString();

            MaxFatsTB.Text = _settings.MaxFats.ToString();
            MinFatsTB.Text = _settings.MinFats.ToString();

            MaxCarbohydratesTB.Text = _settings.MaxCarbohydrates.ToString();
            MinCarbohydratesTB.Text = _settings.MinCarbohydrates.ToString();

            MaxProteinesTB.Text = _settings.MaxProteines.ToString();
            MinProteinesTB.Text = _settings.MinProteines.ToString();

            Products.Items.AddRange(_settings.Banned.ToArray());
        }

        private void AddProduct_Click(object sender, EventArgs e)
        {
            Products.Items.Add(AddProductTB.Text);
            AddProductTB.Text = "";
        }

        private void DeleteProduct_Click(object sender, EventArgs e)
        {
            Products.Items.Remove(Products.SelectedItem);
        }

        private void SaveSettings_Click(object sender, EventArgs e)
        {
            var list = new List<string>();
            foreach (var item in Products.Items)
            {
                list.Add(item.ToString());
            }
            _settings.Banned = list;

            if (double.TryParse(MaxCaloriesTB.Text, out var maxCalories))
            {
                _settings.MaxCalories = maxCalories;
            }
            if (double.TryParse(MinCaloriesTB.Text, out var minCalories))
            {
                _settings.MinCalories = minCalories;
            }
            if (double.TryParse(MaxFatsTB.Text, out var maxFats))
            {
                _settings.MaxFats = maxFats;
            }
            if (double.TryParse(MinFatsTB.Text, out var minFats))
            {
                _settings.MinFats = minFats;
            }
            if (double.TryParse(MaxProteinesTB.Text, out var maxProteines))
            {
                _settings.MaxProteines = maxProteines;
            }
            if (double.TryParse(MinProteinesTB.Text, out var minProteines))
            {
                _settings.MinProteines = minProteines;
            }
            if (double.TryParse(MaxCarbohydratesTB.Text, out var maxCarbohydrates))
            {
                _settings.MaxCarbohydrates = maxCarbohydrates;
            }
            if (double.TryParse(MinCarbohydratesTB.Text, out var minCarbohydrates))
            {
                _settings.MinCarbohydrates = minCarbohydrates;
            }

            _settings.Save();
            this.Close();
        }
    }
}
