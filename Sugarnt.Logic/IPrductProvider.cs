﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sugarnt.Logic
{
    public interface IPrductProvider
    {
        Product GetProductByName(string productName);
        Task<Product> GetProductByBarcode(string productBarcode);
    }
}
