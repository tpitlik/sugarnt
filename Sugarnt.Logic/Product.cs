﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sugarnt.Logic
{

    public class Product
    {

        public Product()
        {
            Values = new NutritionalValues();
        }

        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public string Content { get; set; }
        public string ImageUrl { get; set; }

        public NutritionalValues Values { get; set; }

        public class NutritionalValues
        {
            public double Proteines { get; set; }
            public double Fats { get; set; }
            public double Carbohydrates { get; set; }
            public double Calories { get; set; }
        }
    }
}
