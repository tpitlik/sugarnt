﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sugarnt.Logic
{
    public class AnalyzeError
    {
        public enum ErrorTypes
        {
            Baned, Value
        }

        public static AnalyzeError BanedError(string msg)
        {
            return new AnalyzeError(ErrorTypes.Baned, msg);
        }

        public static AnalyzeError ValueError(string msg)
        {
            return new AnalyzeError(ErrorTypes.Value, msg);
        }

        private AnalyzeError(ErrorTypes type, string msg)
        {
            Type = type;
            Message = msg;
        }

        public ErrorTypes Type { get; }
        public string Message { get; }
    }
}
